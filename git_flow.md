# Workflow for Using Git

### Step 1: Initialize the Project

Create a New Directory:

```bash
mkdir my-project
cd my-project
```

Initialize Git:

```bash
git init
```

Create a README File:

```bash
echo "# My Project" > README.md
```

Add the README to Staging:

```bash
git add README.md
```

Commit the Initial Changes:

```bash
git commit -m "Initial commit with README"
```

### Step 2: Create a Remote Repository

Create a Repository on Git Hosting (e.g., GitHub, GitLab).
Link the Remote Repository:

```bash
git remote add origin https://github.com/username/my-project.git
```

### Step 3: Push Initial Changes to Remote

Push the Initial Commit:

```bash
git push -u origin master
```

### Step 4: Create and Manage Issues

Create a New Issue on your repository.

Note the Issue ID for the next steps.

### Step 5: Create a New Branch for Development

- Assign labels to the issue. For example, if you are working on a feature, assign the label `feature`.
- Create a new branch for the issue. The branch name should be descriptive and follow a consistent format. For example, if you are working on a feature related to issue ID `123`, you could name the branch `feature/123-description`.
- When you start working on the issue, assign `In Progress` label to the issue.
- When you finish working on the issue, assign `Ready for Review` label to the issue.
- When the code is reviewed and approved, assign `Ready for QA` label to the issue.
- When the code is tested and verified, assign `Ready for Merge` label to the issue.
- When the code is merged, assign `Closed` label to the issue.

Create a Branch Using Your Username and Issue ID:

- In gitlab, we will use create merge request and create branch from there.

For example, if your username is `username` and the issue ID is `123`, you could create a branch named `feature/username/123-description`.

- In github, we will use the following command to create a new branch.

```bash
git checkout -b feature/username/issue_id-description
```

### Step 6: Make Changes and Commit

Make Changes to Your Files.
Stage Your Changes:

```bash
git add .
```

Commit Your Changes:

```bash
git commit -m "Implement feature related to issue_id"
```

### Step 7: Rebase Before Pushing

Fetch the Latest Changes from Remote:

```bash
git fetch origin
```

Rebase Your Branch onto the Latest Develop/Master:

```bash
git rebase origin/master
```

Resolve any conflicts if they arise, then continue the rebase:

```bash
git rebase --continue
```

### Step 8: Push the Feature Branch

Push the Feature Branch to Remote:

```bash
git push -u origin feature/username/issue_id-description
```

### Step 9: Create a Merge Request (Pull Request)

In GitLab: you already have a merge request created when you created a branch. Assign the merge request to the reviewer. Once the reviewer approves the merge request, you can merge the changes or ask the reviewer to merge the changes.

Navigate to the Merge Requests Tab.

Create a New Merge Request:

Select your feature/username/issue_id-description branch and the target branch (e.g., master).

Fill in the Details and submit the merge request.

### Step 10: Review and Merge the Changes

Review the Merge Request.

Merge the Request once approved.

### Step 11: Clean Up

Delete the Feature Branch Locally:

```bash
git checkout master
git branch -d feature/username/issue_id-description
```

Delete the Feature Branch Remotely:

```bash
git push origin --delete feature/username/issue_id-description
```
