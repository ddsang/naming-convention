# Git guideline

This document is intended for people using gitlab.  
Each section of the document shows who it is intended for.

| Gitlab Role | Detail                                                     |
| ----------- | ---------------------------------------------------------- |
| developer   | For those who changes content of the repository.           |
| maintainer  | For those who is responsible for maintining the repository |
| owner       | Those who created the repository                           |

Note: It is likely that maintainer is also a developer. Similarly for owner.

---

## Setup note

Intended for: `developer`

Follow the instruction at gitlab, or ask your team mates.

[Reference at Gitlab](https://docs.gitlab.com/ee/ssh/)

- Make sure you use `ed25519` key for better security.

```
ssh-keygen -t ed25519 -C "your connectiv email address"
```

- Make sure to set proper name in your local git

```
git config --global user.name "Firstname Middlename Surname"
```

---

## Branches

Intended for: `developer`, `maintainer`

`[feature/fix/hotfix/chore]/[username]/[branch name]`

There should be mainly 5 types of branches.

| type    | branch name                        | commit/push | options            | purpose                          |
| ------- | ---------------------------------- | ----------- | ------------------ | -------------------------------- |
| feature | `(issue-number)-issue-description` | developper  |                    | work on issue                    |
| develop | develop                            | maintainer  | default, protected | reviewed and merged code         |
| stage   | stage                              | maintainer  | protected          | exact working copy of production |
| prod    | master                             | maintainer  | protected          | production enviroment source     |
| hotfix  | `(issue-number)-hotfix`            | maintainer  | protected          | Quick fix for severe bug in prod |

Optionally you can use `release tag` to manage **release versions** at specific source point.

### feature branch => develop branch

1. Developer start the issue by creating the merge request first.

    > From the issue page, there is button `Create merge request`. Click it to create the WIP merge request as well as the branch with name associated with issue name automatically. E.g. `(issue-number)-issue-title`

    > Do not name the feature branch by your self!

2. Developer commits the changes to the branch.

3. When it's done turn off the WIP status. Make sure the merge request is assigned to the merger.

4. Maintainer reviews code and merge.

```
Side note：  
On the issue page, discuss anything generally related to the issue.
On the merge request page, discuss about the code changes/technical aspect. Usually with between the developer and the reviewer.
```

### develop branch => stage branch

1. Do series of test on develop, and when the quality is sufficient, maintainer merge to the staging.

### stage branch => prod branch

1. After manual review at staging (often done by client/PM), and when it's ready for production, maintainer merge to prod.

### hotfix branch => staging branch => prod branch => develop branch

1. When there is severe bug found at production. Create hotfix branch and fix the bug.
2. Merge to the stage branch and make sure the bug is fixed. Do series of tests.
3. When it works fine on stage branch, merge it to prod branch.
4. Merge it to develop.

---

## Commit comment

Intended for: `developer`

### format

```
[type](#issue id) one line of summary, especially reasons.
details(optional and multiple rows is ok)
```

- (#issue id) id is optional

Type is either following:

| type        | case                                                                                                   |
| ----------- | ------------------------------------------------------------------------------------------------------ |
| feat        | When adding a new feature                                                                              |
| fix         | When fixing a bug                                                                                      |
| docs        | Any changes about documentation                                                                        |
| style       | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| refactor    | A code change that neither fixes a bug nor adds a feature                                              |
| performance | A code change that improves performance                                                                |
| security    | A code change that improves security                                                                   |
| test        | Adding missing or correcting existing tests                                                            |
| chore       | Changes to the build process or auxiliary tools and libraries such as documentation generation         |



example:

```
fix(#312): fixes buttons not working at login page.
This occurs when user first visted the page.
Buttons were disabled by CSS so enabled it.
```

## Gitlab's issue status(issue labels)

Intended for: `developer`

### `Open`

* This is the first state when the issue is created.

### `In Progress` 

* Start working on the issue by adding label `In Progress`. (Or equivalanetly move the list on the issue board GUI.)
* Needs to create the feature branch for this issue and commit update/fixes on the branch.

### `Need Review` 

* When the issue needs review, change label to `Need Review`.
* After feedback, if member needs review again, move issue to `Need Review`.

### `Feedback` 

* After the review, if theres some feed back, move issue to `Feedback`.

### `Closed` 

* After the review, if the issue is complete, then close the issue.
